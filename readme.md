# fileuploader.io

##TIG PHP developer technical test

##Name of your chosen PHP framework and why?

I have chosen Laravel 5.4 as my PHP framework for this project, I believe Laravel has a reputation of been light weight and maintainable framework . Also, I have personally experienced Laravel also has maintained a good source of documentation and strong security features e.g. ''CSRF token", in addition, in my own experience it is easy to build prototype systems much faster and easy to implement a mechanism quick as possible with unit testing.

According to the instructions, this project required a prototype model which required to be maintainable and build up quickly and with stable features, considering all the facts I have decided to use Laravel 5.4 (thought of choosing a very best stable version at the moment based on experience), using a state of art framework such as Laravel would be an ideal solution for this project.

##Steps needed to setup the solution and dependencies

If you want to get a copy of this Git repository, use git clone command.

$ git clone git@bitbucket.org:asalanka/fileuploader.io.git


Create a Host entry in your local server Hosts file e.g. fileuploader.io

Create a Virtual host for the predefined host e.g. fileuploader.io in your conf file e.g. httpd-vhosts.conf


<VirtualHost *:8888>
    ServerName fileuploader.io
    ServerAlias www.fileuploader.io
    DocumentRoot /Applications/MAMP/htdocs/fileuploader/public/
</VirtualHost>


Create a database under the name tig in your mySql database server, or otherwise change the DB_DATABASE parameter in
.env file and recreate a new one, then change the database credentials in DB_USERNAME and DB_PASSWORD with your mySQL credentials.

On your command line tool go inside of the fileuploader folder and run composer install to install Laravel dependancies.

On your command line tool go inside of the fileuploader folder and run  php artisan migrate or if you want to refresh  
php artisan migrate:refresh

Check on mySql whether you have Laravel default tables and product table created in the database then by typing
fileuploader.io (or the defined hostname) you will be able to access the system.

Install intervention package using composer.json, intervention is a image handling and manipulation library with support for Laravel integration.

e.g. "intervention/image": "dev-master"

Install AWS SDK for PHP and Laravel, The AWS SDK for PHP makes it easy for developers to access Amazon Web Services in their PHP code, and build robust applications and software using services like Amazon S3.

e.g. "aws/aws-sdk-php": "3.24.4"

Load new configuration to Laravel application by running 'php artisan config:clear'
