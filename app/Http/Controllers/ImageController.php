<?php

namespace App\Http\Controllers;
use Storage;
use Validator;
use App\FileUpload;

use Illuminate\Http\Request;

class ImageController extends Controller
{
    //Store output to public directory as well as to s3

    public function store(Request $request)
    {

      $rules = array(
            'image' => 'required'
        );

      $validator =  Validator::make($request->all(), $rules);

       if ($validator->fails()) {
         $messages = $validator->messages()->all();

         return response()->json($messages[0], 200);
       }


      if($request->get('image'))
       {
          $image = $request->get('image');

          $ext = explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];

            //check the extension
            if($this->getExtensionType($ext)){
              $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
              \Image::make($request->get('image'))->save(public_path('images/').$name);
            }else{
              return response()->json('Plese upload a valid file type.', 200);
            }

        }

       $image= new FileUpload();
       $image->image_name = $name;
       $image->save();


       $imageLocalPath = public_path('images/').$name;
       Storage::disk('s3')->put('images/'.$name, file_get_contents($imageLocalPath));

       return response()->json('You have successfully uploaded an image!.', 200);
    }

    //Getting the extension type of the image or allowing a particular type of file to upload

    public function getExtensionType($extension)
    {
      $ExtList = array('jpg','jpeg','png', 'gif', 'bmp');

      //Check the extension for validity
      if(!in_array($extension, $ExtList)){
       return false;
      }

      return true;
    }
}
